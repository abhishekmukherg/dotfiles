S.cfga({
    defaultToCurrentScreen: true
})

var fullScreen = S.op("move", {
    "x": "screenOriginX",
    "y": "screenOriginY",
    "width": "screenSizeX",
    "height": "screenSizeY"
})

var leftBar = S.op("push", {
    "direction": "left",
    "style": "bar-resize:screenSizeX/2"
})
var rightBar = S.op("push", {
    "direction": "right",
    "style": "bar-resize:screenSizeX/2"
})

var topLeft = S.op("corner", {
    "direction": "top-left",
    "width": "screenSizeX/2",
    "height": "screenSizeY/2"
})
var bottomLeft = topLeft.dup({ "direction": "bottom-left" })
var topRight = topLeft.dup({ "direction": "top-right" })
var bottomRight = topLeft.dup({ "direction": "bottom-right" })

var relaunch = S.op("relaunch")

var prevScreen = S.op("throw", {"screen": "next"})
var nextScreen = S.op("throw", {"screen": "previous"})

// var screenLeft = S.op("move", {
//     "x": "windowTopLeftX",
//     "y": "windowTopLeftY",
//     "width": "windowSizeX",
//     "height": "windowSizeY",
//     "screen": "previous"
// })
// var screenRight = screenLeft.dup({"screen": "next"})

S.bnda({
    "k:cmd;ctrl": fullScreen,
    "j:cmd;ctrl": leftBar,
    "l:cmd;ctrl": rightBar,

    "u:cmd;ctrl": topLeft,
    "m:cmd;ctrl": bottomLeft,

    "o:cmd;ctrl": topRight,
    ".:cmd;ctrl": bottomRight,

    "r:cmd;ctrl;shift": relaunch,

    "j:cmd;ctrl;shift": prevScreen,
    "l:cmd;ctrl;shift": nextScreen

    // "j:cmd,ctrl,shift": screenLeft,
    // "l:cmd,ctrl,shift": screenRight
})
