command! PackUpdate packadd minpac | source $MYVIMRC | redraw | call minpac#update()
command! PackClean  packadd minpac | source $MYVIMRC | call minpac#clean()

if !exists('*minpac#init')
    finish
endif

" Required:
packadd minpac
call minpac#init()

" Utility used by surround, unimpaired
call minpac#add('tpope/vim-repeat')

" Magic indent detector
call minpac#add('tpope/vim-sleuth')

" Undo tree
call minpac#add('sjl/gundo.vim')

" Airline
call minpac#add('vim-airline/vim-airline')
call minpac#add('vim-airline/vim-airline-themes')

" Navigation
call minpac#add('tpope/vim-vinegar')
call minpac#add('tpope/vim-unimpaired')
call minpac#add('junegunn/fzf')
call minpac#add('junegunn/fzf.vim')

" Filetypes
call minpac#add('fatih/vim-go')
call minpac#add('pearofducks/ansible-vim')
call minpac#add('leafgarland/typescript-vim')
call minpac#add('peitalin/vim-jsx-typescript')

" Text formatting
call minpac#add('junegunn/vim-easy-align')
call minpac#add('tpope/vim-commentary')
call minpac#add('tpope/vim-surround')

" vim notes
call minpac#add('vimwiki/vimwiki', { 'branch': 'dev' })

" Visual formatting
call minpac#add('w0rp/ale')

" Source control
call minpac#add('tpope/vim-fugitive')

" Job control
call minpac#add('tpope/vim-dispatch')

" Colors!
call minpac#add('chriskempson/base16-vim')

" Shougo
"call minpac#add('Shougo/vimproc.vim', {'do': 'silent! !make'})

" {{{ coc
call minpac#add('neoclide/coc.nvim', { 'branch': 'release'})
call minpac#add('honza/vim-snippets')
" }}}
