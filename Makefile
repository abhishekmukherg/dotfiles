all::
	stow --target="$(HOME)" --verbose=3 \
		--stow bin \
		--stow git \
		--stow ssh \
		--stow utils \
		--stow vim \
		--stow x11 \
		--stow slate \
		--stow zsh \
		--stow ranger

check::
	chkstow --target "$(HOME)" --badlinks
