# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

declare -i ZSH_RUN_PROFILE=${ZSH_RUN_PROFILE:-0}
if [[ $ZSH_RUN_PROFILE -eq 1 ]]; then
  zmodload zsh/zprof
fi

source ~/.zinit/bin/zinit.zsh
zinit ice depth=1; zinit light romkatv/powerlevel10k

() {
  alias fig='docker-compose'
  if (( $+commands[exa] )); then
    alias ls="exa"
    alias tree="exa --tree"
  fi

  alias t=task
}

() {
  bindkey "^A" beginning-of-line
  bindkey "^E" end-of-line

  autoload -z edit-command-line
  zle -N edit-command-line
  bindkey -M vicmd v edit-command-line
}

() {
  export HISTFILE=~/.zsh/history
  export HISTFILESIZE=1000000000
  export HISTSIZE=1000000000
  setopt INC_APPEND_HISTORY
  setopt EXTENDED_HISTORY          # Write the history file in the ':start:elapsed;command' format.
  # setopt SHARE_HISTORY             # Share history between all sessions.
  setopt HIST_EXPIRE_DUPS_FIRST    # Expire a duplicate event first when trimming history.
  setopt HIST_IGNORE_DUPS          # Do not record an event that was just recorded again.
  setopt HIST_IGNORE_ALL_DUPS      # Delete an old recorded event if a new event is a duplicate.
  setopt HIST_FIND_NO_DUPS         # Do not display a previously found event.
  setopt HIST_IGNORE_SPACE         # Do not record an event starting with a space.
  setopt HIST_SAVE_NO_DUPS         # Do not write a duplicate event to the history file.
  setopt HIST_VERIFY               # Do not execute immediately upon history expansion.
  setopt HIST_BEEP                 # Beep when accessing non-existent history.
}

() {
  local fasd_dir="${XDG_CACHE_HOME:-$HOME/.cache}/fasd"
  local fasd_cache="$fasd_dir/init"
  if [[ ! -d "$fasd_dir" ]]; then
    mkdir -p "$fasd_dir"
  fi
  if [[ "$commands[fasd]" -nt "$fasd_cache" || ! -s "$fasd_cache" ]]; then
    fasd --init posix-alias zsh-hook zsh-ccomp zsh-ccomp-install > "$fasd_cache"
  fi
  source "$fasd_cache"
  alias j='fasd_cd -d'
}

() {
  [[ -f ~/.wzshrc ]] && source ~/.wzshrc
}


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
